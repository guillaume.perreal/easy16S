library(microbenchmark)

mb <- microbenchmark::microbenchmark(
  "clear" = {
    rm(list = ls())
    lapply(paste('package:', names(sessionInfo()$otherPkgs), sep=""), detach, character.only=TRUE, unload=TRUE)
    },
  "load packages" = {
    library(shinydashboard)
    library(glue)
    source("https://raw.githubusercontent.com/mahendra-mariadassou/phyloseq-extended/master/R/load-extra-functions.R")
    source("internals.R")
  },
  "load demo Chailloux" = {
    load("demo/demo.RData")
    data <- get("food")
  },
  "barplot" = {
    p <- plot_bar(physeq = data, fill = "Phylum", x = "Description", title = "OTU abundance barplot")
    p <- p + facet_grid(". ~ EnvType", scales = "free_x")
    plot(p)
  },
  "filtered plot" = {
    p <- plot_composition(physeq = data, taxaRank1 = "Kingdom", taxaSet1 = "Bacteria", taxaRank2 = "Phylum", numberOfTaxa = 10, fill = "Phylum", x = "Description")
    p <- p + facet_grid(". ~ EnvType", scales = "free_x")
    plot(p)
  },
  "heatmap" = {
    p <- plot_heatmap(prune_taxa(names(sort(taxa_sums(data), decreasing = TRUE)[1:250]), data), distance = "bray", method = "NMDS", low = "yellow", high = "red", na.value = "white", sample.order = "Description", title = "Taxa heatmap by samples")
    p <- p + facet_grid(". ~ EnvType", scales = "free_x")
    plot(p)
  },
  "alpha" = {
    p <- plot_richness(physeq = data, measures = c("Observed", "Chao1", "ACE", "Shannon", "Simpson", "InvSimpson", "Fisher"), x = "EnvType", color = "EnvType", shape = "FoodType", title = "Alpha diversity graphics")
    p <- p + geom_boxplot()
    p <- p + geom_point()
    plot(p)
  },
  "beta" = {
    beta <- melt(as(distance(data, method = "bray"), "matrix"))
    colnames(beta) <- c("x", "y", "distance")
    new_factor = as.factor(get_variable(data, "EnvType"))
    variable_sort <- as.factor(get_variable(data, "EnvType")[order(new_factor)])
    L = levels(reorder(sample_names(data), as.numeric(new_factor)))
    beta$x <- factor(beta$x, levels = L)
    beta$y <- factor(beta$y, levels = L)
    palette <- hue_pal()(length(levels(new_factor)))
    tipColor <- col_factor(palette, levels = levels(new_factor))(variable_sort)
    p1 <- ggplot(beta, aes(x = x, y = y, fill = distance))
    p1 <- p1 + geom_tile()
    p1 <- p1 + ggtitle("Beta diversity heatmap")
    p1 <- p1 + theme(axis.text.x = element_text(angle = 90, hjust = 1, color = tipColor), axis.text.y = element_text(color = tipColor), axis.title.x = element_blank(), axis.title.y = element_blank())
    plot(p1 + scale_fill_gradient2())
  },
  "rarefaction" = {
    p <- ggrare(physeq = data, step = 100, se = FALSE, color = "EnvType", label = "Description")
    p <- p + facet_grid(". ~ FoodType")
    p <- p + geom_vline(xintercept = min(sample_sums(data)), color = "gray60")
    p <- p + ggtitle("Rarefaction curves")
    plot(p)
  },
  "acp" = {
    p <- plot_samples(physeq = data, ordination = ordinate(data, method = "MDS", distance = "unifrac"), axes = c(1, 2), color = "EnvType", shape = "FoodType", replicate = "EnvType", label = "Description", title = "Samples ordination graphic")
    p <- p + stat_ellipse(aes_string(group = "EnvType"))
    plot(p + theme_bw())
  },
  "tree" = {
    p <- plot_tree(physeq = prune_taxa(names(sort(taxa_sums(data), decreasing = TRUE)[1:20]), data), method = "sampledodge", color = "EnvType", size = "abundance", label.tips = "taxa_names", sizebase = 5, ladderize = "left", plot.margin = 0, title = "Phylogenetic tree")
    plot(p)
  },
  "clustering" = {
    p <- plot_clust(physeq = data, dist = "unifrac", method = "ward.D2", color = "EnvType")
    plot(p)
  },
  times = 100, unit = "s", control = list(order="inorder"))

mb
save(mb, file = "benchmark.RData")

mb_plot <- microbenchmark::autoplot.microbenchmark(mb)
ggsave("benchmark.png", plot = mb_plot)
