ARG R_TAG=3.5.2
FROM rocker/shiny:$R_TAG AS builder

# Install and build R packages from CRAN and BioConduit, removing documentation and tests
RUN R -e 'install.packages( \
    c("BiocManager", "DT", "dplyr", "factoextra", "glue", "microbenchmark", "shinycustomloader", "shinydashboard"), \
    Ncpus='$(nproc)', \
    clean=1, \
    )' \
    && R -e 'BiocManager::install("phyloseq", Ncpus='$(nproc)', clean=1)' \
    && rm -rf /usr/local/lib/R/*/*/{doc,html,help,unitTests}

# Use Yelp/dumb-init as entrypoint to properly handle Unix signals
ARG DUMB_INIT_VERSION=1.2.2
ADD https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}/dumb-init_${DUMB_INIT_VERSION}_amd64 /sbin/dumb-init
RUN chmod +x /sbin/dumb-init
ENTRYPOINT ["/sbin/dumb-init", "--"]

# Start the shiny server by default
CMD ["/usr/bin/shiny-server.sh"]

# Clear /srv/shiny-server/ and create /var/lib/shiny-server
RUN rm -rf /srv/shiny-server/* \
    && mkdir -p var/lib/shiny-server

# Install easy16S sources and resource
ADD *.R /srv/shiny-server/
ADD www /srv/shiny-server/www

# Build demo data
ADD demo /tmp/demo
RUN cd /tmp/demo \
    && R -f make-demo.R \
    && mkdir -p /srv/shiny-server/demo \
    && ls -la && cp demo.RData /srv/shiny-server/demo

# Fix file permissions
RUN chmod -R a+rX /srv/shiny-server/ \
    && chgrp shiny /var/lib/shiny-server \
    && chmod g+rwxs /var/lib/shiny-server

# Run with a non-privileged user
USER shiny
